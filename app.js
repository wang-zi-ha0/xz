//引入express   
const express=require('express')
const userRouter=require('./routes/user.js')
//创建web服务器
const app=express();
//设置端口
app.listen(8080)
//将post请求的数据解析为对象
app.use(express.urlencoded({
         extended: false
}))
//挂载到服务器中,添加前缀
app.use('/v1/users',userRouter);
app.use((err,req,res,next)=>{
        //接收err错误
		console.log(err);
		//设置状态码
        res.status(505).send({code: 505,msg:'服务端错误'});
})
