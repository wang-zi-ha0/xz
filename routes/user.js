const express=require('express')
const mysql=require('./pool.js')
const user=express.Router()
let phone=/^(13[0-9]|14[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9])\d{8}$/;
let email=/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
let upwd=/^[a-zA-Z]\w{5,17}$/;
let uname=/^[a-zA-Z][a-zA-Z0-9_]{4,15}$/;
user.post('/reg',(req,res,next)=>{
	   console.log(req.body);
	   //判断是否为空
	   if(!req.body.uname){
	     res.send({code:401,msg:'uname not null'})
	      return;
		 }
	   if(!req.body.upwd){
	      return;
		 }
	   if(!req.body.email){
	      return;
		 }
	   if(!req.body.phone){
	      return;
		 }
			//判断输入格式是否正确
		 if(!phone.test(req.body.phone)){
		   res.send({code:405 ,msg:'手机号格式错误'})
		  return;
		   }
		 if(!email.test(req.body.email)){
		   res.send({code:405 ,msg:'邮件格式错误'})
		  return;
		   }
		 if(!uname.test(req.body.uname)){
		   res.send({code:405 ,msg:'账号格式错误'})
		  return;
		   }
		 if(!upwd.test(req.body.upwd)){
		   res.send({code:405 ,msg:'密码格式错误'})
		  return;
		   }
		mysql.query('insert into xz_user set?',[req.body],(err,s)=>{
		       if(err){
			   next(err);
			   return;
			   } 
			   console.log(s);
			   res.send({code: 200,msg: '注册成功'});
		});
})
user.post('/login',(req,res,next)=>{
       let obj=req.body;
	   console.log(obj);
	   //验证数据是否为空
	   if(!obj.uname){
	   res.send({code:401,msg:'空账户'})
		return;
	   }
	   if(!obj.upwd){
	   res.send({code:402,msg:'空密码'}) 
	    return;
	   }
	
	   //执行sql语句，用占位符
	   mysql.query('select * from xz_user where uname=? and upwd=?',[obj.uname,obj.upwd],(err,s)=>{
            //如果有错误执行下一个中间件
			 if(err){
			next(err)
		    //阻止下列代码执行
		     return;
			 }
			 //正确结果
          console.log(s);
		  //结果为数组，判断数组下的长度，如果为0，登陆失败，否则成功。
		if(s.length!==0){
	     res.send({code: 200,msg:'登陆成功'})
        }else{
		 res.send({code: 201,msg:'登陆失败'})
		}
	   })		  
})
user.put('/',(req,res,next)=>{
	     let jpg=req.body;
		 console.log(jpg);
		 //修改的值不能为空
		  let i = 400;  //初始化，保存状态码
		 //遍历对象得到每个对象的值
		 for(let k in jpg){
			i++;  //每循环一次，自增
		  //判断属性值是否为空，如果为空则提示该属性名不能为空
		  if(!jpg[k]){
		       res.send({code: i,msg: k+"不能为空"}) 
			   return;
		  }      
		 } 
		 //执行sql命令
		 mysql.query('update xz_user set?where uid=?',[jpg,jpg.uid],(err,s)=>{
		         if(err){
				  next(err);
				  return;
				 }
				 console.log(s);
				 //结果为对象，判断对象下的affectedRows,如果是0说明修改失败，否则修改成功
				 if(s.affectedRows>0){
				 res.send({code :200,msg:'修改成功'})
				 }else{
				 res.send({code :201,msg:'修改失败'})
				 }
		 })		 
})

user.get('/',(req,res,next)=>{
	  let png=req.query;
      console.log(png)
		  //添加默认值
	 if(!png.pna) png.pna=1;
	 if(!png.count) png.count=5;
	    //开始的值
	  let kai=(png.pna-1)*png.count
	   //执行sql命令
	  mysql.query('select * from xz_user limit ?,?',[kai,parseInt(png.count)],(err,s)=>{
	        if(err){
			 next(err)
              return;
			}
			console.log(s);
			res.send({code:200,msg: '查询成功',data:s})
	  })
})	
user.delete('/',(req,res,next)=>{
           	  let gif=req.query;
			  console.log(gif)
			   //验证是否为空
			  if(!gif.uid) res.send({code:401,msg:'该用户不存在'})
			  mysql.query('delete from xz_user where uid=?',[gif.uid],(err,s)=>{
			          if(err){
					   next(err)
					   return;
					  }  
					  console.log(s);
					  if(s.affectedRows>0){
					   res.send({code:200,msg:'删除成功'})
					  }else{
					   res.send({code:201,msg:'没有该数据'}) 
					   }
			  })
})
user.get('/:uid',(req,res,next)=>{
         let jpeg=req.params;
		 console.log(jpeg);
		 mysql.query('select * from xz_user where uid=?',[jpeg.uid],(err,s)=>{
		             if(err){
					  next(err)
						return;
					 }
					 console.log(s);
					 if(s.length===0){
				     res.send({code :401,msg:'该用户不存在'})
					 }else{
					   res.send({code :200,msg:'查询成功',data: s})
					 }
		 })
})
user.get('/check/uname',(req,res,next)=>{
         let uname=req.query;
		 console.log(uname)
		 mysql.query('select uname from xz_user where uname=?',[uname.uname],(err,s)=>{
		         if(err){
				  next(err)
                  return;
				 }
				 console.log(s)
					if(s.length==0){
				   res.send({code:401,msg:'用户名不存在'})
				 }else{
				   res.send({code :200,msg:'查询成功', data: s})
				   }
		 })
})

module.exports=user;
 